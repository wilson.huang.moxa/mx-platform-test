# Moxa Automatic Test
## Prerequisite
```
$ pip install -r requirements.txt
```

## Run test
- Run all tests of model
  ```
  $ pytest --model=<model name>
  ```
- Run specific test
  ```
  $ pytest --model=<model name> tests/test_cpu.py::test_get_info
  ```
## How to add new model
- Add a new associative array to model.yml

  If the new model is under the tests, its value should be `yes`, otherwise `no`.

  ```yaml
  models:
  ...
    <new model name>:
      cpu: yes
      memory: yes
      disk: yes
      usb: yes
      ethernet: yes
      serial: yes
      digial_io: yes
      wifi: yes
      cellular: yes
      watchdog: yes
      time: yes
      apt_server: yes
      tpm: yes
      gps: yes
      gpio: yes
      filesystem: yes
      audio: yes
      video: yes
      nmea: yes
      eeprom: yes
      dip_switch: yes
  ```

  If there are some additional information for tests, it could be presented as below.

  ```yaml
  models:
  ...
    <new model name>:
      cpu:
        model_name: "Intel(R) Celeron(R) 6305E @ 1.80GHz"
        cpu_cores: "2"
        bogomips: "3609.60"
  ```

## How to add new tests
1. Add <new_tests>.py to `tests/fixtures/` and register to `tests/conftest.py`
    ```python
    @pytest.fixture(scope="class")
    def model_<tests>_spec(model_spec):
      return model_spec["<tests>"]

    @pytest.fixture(scope="class")
    def is_under_<tests>_tests(model_<tests>_spec, model_name):
      if not model_<tests>_spec:
        pytest.skip(f"Skip run tests on model {model_name}")
    ```
    ```python
    pytest_plugins = [
      ...
      "tests.fixtures.<tests>",
    ]
    ```
2. Add test_<new_tests>.py to `tests/`
    ```python
    @pytest.mark.usefixtures("is_under_<tests>_tests")
    class Test_<tests>:
      def test_1(self):
      ...
      def test_2(self):
      ...
    ```
3. Add new key:value to `model.yml`
    ```yaml
    models:
    ...
      <model name>:
        ...
        <tests>: yes
    ```

## To do
- [ ] Complete mc-3201-tl test cases
- [ ] Integrate to gitlab CI/CD