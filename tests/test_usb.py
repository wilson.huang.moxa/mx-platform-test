import pytest

@pytest.mark.usefixtures("is_under_usb_tests")
class Test_USB:
    @pytest.mark.intergration
    def test_usb_performance(self, run_usb_performance):
        for test in run_usb_performance():
            assert test.returncode == 0

    @pytest.mark.intergration
    def test_usb_stick_automount(self):
        pytest.skip()

    @pytest.mark.component
    def test_usb_detection(self):
        pytest.skip()

    @pytest.mark.component
    def test_get_usb_info(self):
        pytest.skip()