import pytest

@pytest.mark.usefixtures("is_under_disk_tests")
class Test_Disk:
    @pytest.mark.component
    def test_stress_ng(self, run_disk_stress_ng):
        assert run_disk_stress_ng.returncode == 0

    @pytest.mark.component
    def test_disk_performance(self, run_dd_performance):
        for test in run_dd_performance():
            assert test.returncode == 0

    @pytest.mark.component
    def test_get_partition_table(self, run_fdisk):
        assert run_fdisk.returncode == 0

    @pytest.mark.component
    def test_get_filesystem_space(self, run_df):
        assert run_df.returncode == 0

    @pytest.mark.component
    def test_get_block_device(self, run_lsblk):
        assert run_lsblk.returncode == 0

    @pytest.mark.performance
    def test_fio_sequential_speed(self, run_fio):
        assert run_fio.returncode == 0

    @pytest.mark.performance
    def test_filesystem_benchmark(self, run_iozone):
        assert run_iozone.returncode == 0




