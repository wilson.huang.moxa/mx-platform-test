import pytest

@pytest.mark.usefixtures("is_under_memory_tests")
class Test_Memory:
    @pytest.mark.component
    def test_get_info(self, get_mem_info, model_memory_spec):
        mem_size_kb = int(get_mem_info["MemTotal"])

        if mem_size_kb == 0:
            assert False

        mem_size_gb = round(mem_size_kb / 1024 / 1024)

        assert mem_size_gb == model_memory_spec["total_size"]

    @pytest.mark.performance
    def test_stress_ng(self, run_memory_stress_ng):
        assert run_memory_stress_ng.returncode == 0

    @pytest.mark.performance
    def test_memtester(self, run_memtester):
        assert run_memtester.returncode == 0