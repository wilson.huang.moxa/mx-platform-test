import pytest

@pytest.mark.usefixtures("is_under_cpu_tests")
class Test_CPU:
    @pytest.mark.component
    def test_get_info(self, get_cpu_info, model_cpu_spec):
        info = get_cpu_info

        assert info['model name'] == model_cpu_spec["model_name"]
        assert info['cpu cores'] == model_cpu_spec["cpu_cores"]
        assert info['bogomips'] == model_cpu_spec["bogomips"]

    @pytest.mark.component
    def test_stress_ng(self, run_cpu_stress_ng):
        assert run_cpu_stress_ng.returncode == 0