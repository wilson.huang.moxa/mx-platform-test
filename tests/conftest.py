pytest_plugins = [
    "tests.fixtures.common",
    "tests.fixtures.tools",
    "tests.fixtures.cpu",
    "tests.fixtures.memory",
    "tests.fixtures.disk",
    "tests.fixtures.usb",
]