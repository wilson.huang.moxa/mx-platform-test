import pytest
import yaml
import subprocess
import shlex
import datetime
from pathlib import Path

@pytest.fixture
def log_path(model_name):
    day = datetime.datetime.now().strftime("%Y-%m-%d")
    path = Path(f"{Path.cwd()}/log/{model_name}/{day}")

    if not path.is_dir():
        path.mkdir(parents=True, exist_ok=True)

    return path.resolve()

def pytest_addoption(parser):
    parser.addoption(
        "--model",
        action="store",
        type=str,
        help="Please input which model you want to test."
    )

@pytest.fixture(scope="session")
def model_name(request):
    return request.config.getoption("--model")

@pytest.fixture(scope="session")
def open_yaml_file():
    try:
        with open("model.yml", "r") as fp:
            yaml_file = yaml.load(fp, Loader=yaml.CLoader)
        return yaml_file
    except FileNotFoundError as e:
        pytest.exit(msg="Cannot find model.yml.")

@pytest.fixture(scope="session")
def model_env(open_yaml_file):
    return open_yaml_file["env"]

@pytest.fixture(scope="session")
def model_spec(open_yaml_file, model_name):
    models = open_yaml_file["models"]
    try:
        return models[model_name]
    except KeyError:
        pytest.exit(msg=f"Unknown model name: {model_name}. Available model: {', '.join(list(models.keys()))}")

@pytest.fixture
def sudo_command(model_env):
    def sudo(cmd):
        passwd =  subprocess.Popen(["echo", model_env["su_passwd"]], stdout=subprocess.PIPE)
        proc = subprocess.run(shlex.split(f"sudo -S {cmd}"), stdin=passwd.stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        return proc
    return sudo