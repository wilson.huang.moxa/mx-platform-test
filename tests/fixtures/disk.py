import pytest
import subprocess
import shlex
import tempfile

@pytest.fixture(scope="class")
def model_disk_spec(model_spec):
    return model_spec["disk"]

@pytest.fixture(scope="class")
def is_under_disk_tests(model_disk_spec, model_name):
    if not model_disk_spec:
        pytest.skip(f"Skip run tests on model {model_name}")

@pytest.fixture
def run_disk_stress_ng(stress_ng, log_path):
    options = f"-d 90 \
                --hdd-bytes 1048576 \
                --hdd-write-size 100 \
                --timeout 6000s \
                -v --metrics --timestamp \
                --log-file {log_path}/disk-stress-ng.log"

    return stress_ng(options)

@pytest.fixture
def run_dd_performance(dd, log_path):
    def dd_performance():
        with open(f"{log_path}/disk-dd-perf.log", "w") as fp:
            temp_dir = tempfile.TemporaryDirectory()
            temp_file = f"{temp_dir.name}/dd-perf-file"

            # Write data
            yield dd(f"if=/dev/zero of={temp_file} bs=4k count=1048576", stderr=fp)
            # Read Data
            cmd = "echo 3 > /proc/sys/vm/drop_caches"
            yield subprocess.run(shlex.split(cmd), stdout=subprocess.DEVNULL)
            yield subprocess.run(["sync"], stdout=subprocess.DEVNULL)
            yield dd(f"if={temp_file} of=/dev/null bs=4k count=1048576", stderr=fp)

            temp_dir.cleanup()
    return dd_performance

@pytest.fixture
def run_fdisk(sudo_command, model_disk_spec, log_path):
    cmd = f"fdisk -l {model_disk_spec['storage_device_name']}"
    proc = sudo_command(cmd)

    yield proc

    with open(f"{log_path}/disk-fdisk-partition.log", "wb") as fp:
        if proc.returncode == 0:
            fp.write(proc.stdout)
        else:
            fp.write(proc.stderr)

@pytest.fixture
def run_df(log_path):
    cmd = "df -h"
    proc = subprocess.run(shlex.split(cmd), stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    yield proc

    with open(f"{log_path}/disk-df-space.log", "wb") as fp:
        if proc.returncode == 0:
            fp.write(proc.stdout)
        else:
            fp.write(proc.stderr)

@pytest.fixture
def run_lsblk(log_path):
    cmd = "lsblk"
    proc = subprocess.run(shlex.split(cmd), stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    yield proc

    with open(f"{log_path}/disk-lsblk.log", "wb") as fp:
        if proc.returncode == 0:
            fp.write(proc.stdout)
        else:
            fp.write(proc.stderr)

@pytest.fixture(params=["read", "write"])
def run_fio(fio, request, log_path):
    io_type = request.param
    temp_dir = tempfile.TemporaryDirectory()
    temp_file = f"{temp_dir.name}/fio-tmp-file"

    options = f"--name Sequential-{io_type}-speed \
                --eta-newline=5s \
                --rw={io_type} \
                --size=2g \
                --io_size=10g \
                --blocksize=1024k \
                --ioengine=libaio \
                --fsync=10000 \
                --iodepth=32 \
                --direct=1 \
                --numjobs=1 \
                --runtime=60 \
                --group_reporting \
                --filename={temp_file} \
                --output={log_path}/disk-fio-{io_type}.log"

    yield fio(options)

    temp_dir.cleanup()

@pytest.fixture
def run_iozone(iozone, log_path):
    proc = iozone(f"-a -f /tmp/iozone.tmp")

    yield proc

    with open(f"{log_path}/disk-iozone.log", "wb") as fp:
        if proc.returncode == 0:
            fp.write(proc.stdout)
        else:
            fp.write(proc.stderr)