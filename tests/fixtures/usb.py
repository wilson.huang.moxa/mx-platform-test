import pytest
import subprocess
import shlex
import tempfile

@pytest.fixture(scope="class")
def model_usb_spec(model_spec):
    return model_spec["usb"]

@pytest.fixture(scope="class")
def is_under_usb_tests(model_usb_spec, model_name):
    if not model_usb_spec:
        pytest.skip(f"Skip run tests on model {model_name}")

@pytest.fixture
def run_usb_performance(sudo_command, model_usb_spec, dd, log_path):
    def dd_performance():
        with open(f"{log_path}/usb-dd-perf.log", "w") as fp:
            yield sudo_command(f"mount {model_usb_spec['partition_name']} /mnt")

            temp_dir = tempfile.TemporaryDirectory(dir="/mnt")
            temp_file = f"{temp_dir.name}/dd-perf-file"

            # Write data
            yield dd(f"if=/dev/zero of={temp_file} bs=4k count=1048576", stderr=fp)
            # Read data
            cmd = "echo 3 > /proc/sys/vm/drop_caches"
            yield subprocess.run(shlex.split(cmd), stdout=subprocess.DEVNULL)
            yield subprocess.run(["sync"], stdout=subprocess.DEVNULL)
            yield dd(f"if={temp_file} of=/dev/null bs=4k count=1048576", stderr=fp)

            temp_dir.cleanup()
            yield sudo_command(f"umount /mnt")
    return dd_performance