import pytest
import subprocess
import shlex
import shutil

class ShellCommand:
    def __init__(self, command):
        if not isinstance(command, str):
            raise TypeError("command argument must be string type.")

        if not shutil.which(command):
            raise Exception(f"command '{command}' is not exist.")

        self.command = command
        print(self.command)

    def __call__(self, options, stdout=subprocess.PIPE, stderr=subprocess.PIPE, **kwargs):
        return subprocess.run(shlex.split(f"{self.command} {options}"), stdout=stdout, stderr=stderr, **kwargs)

@pytest.fixture
def stress_ng():
    return ShellCommand("stress-ng")

@pytest.fixture
def dd():
    return ShellCommand("dd")

@pytest.fixture
def memtester():
    return ShellCommand("memtester")

@pytest.fixture
def fio():
    return ShellCommand("fio")

@pytest.fixture
def iozone():
    return ShellCommand("iozone")