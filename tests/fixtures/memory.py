import pytest
import subprocess

@pytest.fixture(scope="class")
def model_memory_spec(model_spec):
    return model_spec["memory"]

@pytest.fixture(scope="class")
def is_under_memory_tests(model_memory_spec, model_name):
    if not model_memory_spec:
        pytest.skip(f"Skip run tests on model {model_name}")

@pytest.fixture
def get_mem_info():
    mem_info = {}
    proc = subprocess.Popen(["cat", "/proc/meminfo"], stdout=subprocess.PIPE)

    for line in proc.stdout:
        try:
            (k, v) = line.decode('utf-8').split(':')
            mem_info[k] = v.strip('\n s kB')
        except ValueError:
            pass

    return mem_info

@pytest.fixture
def run_memory_stress_ng(stress_ng, log_path):
    options = f"--vm 8 \
                --vm-bytes 80% \
                --timeout 60m \
                -v --metrics --timestamp \
                --log-file {log_path}/memory-stress-ng.log"

    return stress_ng(options)

@pytest.fixture
def run_memtester(get_mem_info, memtester):
    mem_size_kb = int(get_mem_info["MemAvailable"])
    mem_size_mb = round(mem_size_kb / 1024)
    test_mem_size_mb = round(mem_size_mb * 0.9)

    options = f"{test_mem_size_mb}M 10"

    return memtester(options)