import pytest
import subprocess

@pytest.fixture(scope="class")
def model_cpu_spec(model_spec):
    return model_spec["cpu"]

@pytest.fixture(scope="class")
def is_under_cpu_tests(model_cpu_spec, model_name):
    if not model_cpu_spec:
        pytest.skip(f"Skip run tests on model {model_name}")

@pytest.fixture
def get_cpu_info():
    cpu_info = {}
    proc = subprocess.Popen(["cat", "/proc/cpuinfo"], stdout=subprocess.PIPE)

    for line in proc.stdout:
        try:
            (k, v) = line.decode('utf-8').split(':')
            cpu_info[k.strip('\t')] = v.strip('\n s')
        except ValueError:
            pass

    return cpu_info

@pytest.fixture
def run_cpu_stress_ng(get_cpu_info, stress_ng, log_path):
    cpu_cores = get_cpu_info["cpu cores"]

    options = f"--cpu {cpu_cores} \
                --cpu-load 100 \
                --timeout 600s \
                -v --metrics --timestamp \
                --log-file {log_path}/cpu-stress-ng.log"

    return stress_ng(options)